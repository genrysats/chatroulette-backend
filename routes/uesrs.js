var express = require('express');
var router = express.Router();

const users = require("../controllers/user.controller");
const jwt = require('jsonwebtoken');

const db = require('../models/index');

let validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};


//

const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client('647115761678-9i3g0nl51fveuhvc690o21fotp7q2fk3.apps.googleusercontent.com');
async function verify(token) {
  const ticket = await client.verifyIdToken({
      idToken: token,
      audience: '647115761678-9i3g0nl51fveuhvc690o21fotp7q2fk3.apps.googleusercontent.com',  // Specify the CLIENT_ID of the app that accesses the backend
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  });
  const payload = ticket.getPayload();
  const userid = payload['sub'];
  return payload;
  // If request specified a G Suite domain:
  //const domain = payload['hd'];
}

//

router.post('/google_login', (req,res,next) => {
    if(req.body.token){
        verify(req.body.token).then(ans => {
            users.continueGoogle(ans.email, 'USA', ans.given_name, (data, user=null) => {
                if(!user)
                    return res.json(data);
                
                jwt.sign( { exp: Math.floor(Date.now() / 1000) + (60 * 60), user }, 'secretkey', (err, token) => {
                    if(err) {
                        res.json({'success': false, 'message': 'error...'});
                    }
                    res.json( {...data, token} );
                });

            });

        }).catch(err => {
            res.json({'success': false, 'message': 'Something wrong...'});
        });
    }else{
        res.json({'success': false, 'message': 'Missing token'});
    }
});


router.get('/countries', (req,res,next) => {
    db.sequelize.query("SELECT Code, Name FROM `country`", { type: db.sequelize.QueryTypes.SELECT })
    .then((countries) => {
        res.json({'success': true, 'data': countries});
        return;
      // We don't need spread here, since only the results will be returned for select queries
    }).catch((err) => {
        res.json({'success': false, 'message': 'Try to register later...'});
    });
});

router.post('/login', function(req, res, next) {
    if(!req.body.email || !validateEmail(req.body.email))
        res.json({'success': false, 'message': 'Missing email'});
    if(!req.body.password || req.body.password.length < 5 || req.body.password.length > 20)
        res.json({'success': false, 'message': 'Missing password'});

    users.login(req.body.email, req.body.password, (ans, user=null) => {
        if(!user)
            return res.json(ans);
        
        jwt.sign( { exp: Math.floor(Date.now() / 1000) + (60 * 60), user }, 'secretkey', (err, token) => {
            if(err) {
                res.json({'success': false, 'message': 'error...'});
            }
            res.json( {...ans, token} );
        });

    });
});


router.post('/register', function(req,res,next) {
    if(!req.body.email || !validateEmail(req.body.email))
        res.json({'success': false, 'message': 'Missing email'});
    if(!req.body.password || req.body.password.length < 5 || req.body.password.length > 20)
        res.json({'success': false, 'message': 'Missing password'});
    if(!req.body.country || req.body.country.length !== 3)
        res.json({'success': false, 'message': 'Missing country'});
    if(!req.body.username || req.body.username.length < 4)
        res.json({'success': false, 'message': 'Missing username'});
    
    users.register(req.body.email, req.body.password, req.body.country, req.body.username, (ans, user=null) => {
        if(!user)
            return res.json(ans);
            
        jwt.sign( { exp: Math.floor(Date.now() / 1000) + (60 * 60), user }, 'secretkey', (err, token) => {
            if(err) {
                res.json({'success': false, 'message': 'error...'});
            }
            res.json( {...ans, token} );
        });
        
    });
})

function verifyToken(req, res, next) {
    const token = req.headers['authorization'];

    if(token) {
        jwt.verify(token, 'secretkey', (err, authData) => {
            if(err) return res.json({'success': false, message: 'Wrong token..'});
            req.authData = authData;
            next();
        });
    }else {
        res.json({'success': false, message: 'Wrong token..'});
    }
}

module.exports = router;
