var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/uesrs');

var cors = require('cors');
var app = express();

let waitingQueue = [];
let connected = {  };
let userToSocket = {}; //email => { user: {}, socketID }

app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/public_info', function (req, res) {
  res.json({'success': true, 'connected': Object.keys(connected).length, 'waiting': waitingQueue.length});
});

app.use('/', indexRouter);
app.use('/users', usersRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const db = require("./models/index");
db.sequelize.sync();

app.update = (waitingQueue1, connected1) => {
    waitingQueue = waitingQueue1;
    connected = connected1;
};

module.exports = app;
