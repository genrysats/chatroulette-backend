module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("user", {
      email: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false,
      },
      password: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      username: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      country: {
        type: Sequelize.CHAR,
        references: {
            model: 'country', 
            key: 'Code',
        },
        allowNull: false,
      },
      google: {
        type: Sequelize.CHAR,
        allowNull: false,
        defaultValue: 0
      },
    });
  
    return User;
};