const db = require("../models/index");
const bcrypt = require('bcrypt');

const User = db.users;
const Op = db.Sequelize.Op;
const saltRounds = 10;

exports.continueGoogle = (email, country, username, callback) => {
    User.findOne({
        where: {
            email: email
        }
    }).then(data => {
        if(data.google && data.google == 1) {
            return callback({ success: true, message: 'Successfully logged in' }, data);
        }else {
            return callback({ success: false, message: 'Woops..You will have to login with password' });
        }
    }).catch( () => {
        //If not found, register.
        const user = {
            email, 
            country,
            username,
            google: 1
        };

        User.create(user).then((data) => {
            return callback({ success: true, message: 'User created successfully' }, user);
        }).catch(db.Sequelize.ForeignKeyConstraintError, (err) => {
            return callback({ success: false, message: 'Unknown country'});
        }).catch(err => {
            return callback({ success: false, message: 'Something went wrong, try again later'});
        });
    });


    

    


};

exports.register = async (email, password, country, username, callback) => {
    bcrypt.hash(password, saltRounds, (err, hash) => {
        if(err) return callback({ success: false, message:'Something wrong with this password' });

        const user = {
            email: email,
            password: hash,
            country: country,
            username: username
        };
        
        User.create(user).then((data) => {
            return callback({ success: true, message: 'User created successfully' }, user);
        }).catch(db.Sequelize.ForeignKeyConstraintError, (err) => {
            return callback({ success: false, message: 'Unknown country'});
        }).catch(db.Sequelize.UniqueConstraintError, (err) => {
            return callback({ success: false, message: 'This email already exists'});
        }).catch(err => {
            return callback({ success: false, message: 'Something went wrong, try again later'});
        });

    });
}


exports.login = (email, password, callback) => {
    User.findOne({
        where: {
            email: email
        }
    }).then(data => {
        if(bcrypt.compare(password, data.password, function(err, match) {
            if(err || !match) return callback({ success: false, message: 'Wrong Credentials'});
            return callback({ success: true, message: 'Successfully logged in' }, data);
        }));
    }).catch( () => { return callback({ success: false, message: 'Wrong Credentials' }); });
};